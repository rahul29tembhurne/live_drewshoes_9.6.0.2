﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Cache;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Custom.Cache.Cache
{
    public class DSPublishProductCache : PublishProductCache, IDSPublishProductCache
    {
        #region Private Variable
        private readonly IDSPublishProductService _service;
        #endregion

        #region Constructor
        public DSPublishProductCache(IDSPublishProductService publishProductService) : base(publishProductService)
        {
            _service = publishProductService;
        }
        #endregion

        public DSPublishProductResponse GetStoreLocationDetails(string state, string sku)
        {
            DataTable storelocations = _service.GetStoreLocationDetails(state, sku);

            DSPublishProductResponse response = new DSPublishProductResponse { StoreLocations = storelocations };

            return response;
        }
    }
}
