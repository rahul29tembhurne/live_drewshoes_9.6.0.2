﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Helper
{
    public class DSAttributeSwatchHelper : AttributeSwatchHelper
    {
        public override void GetAssociatedConfigurableProducts(KeywordSearchModel searchModel, SearchRequestModel model, string attributeCode)
        {

            ImageHelper imageHelper = new ImageHelper(model.PortalId);
            var _searchModel = searchModel.Products.Where(x => x.ProductType == ZnodeConstant.ConfigurableProduct).ToList();

            foreach (SearchProductModel searchProduct in _searchModel)
            {                
                    List<AttributesSelectValuesModel> selectValues = GetSelectValues(attributeCode, searchProduct);
                    if (selectValues?.Count > 0)
                    {
                        searchProduct.SwatchAttributesValues = GetSwatchImages(selectValues, imageHelper);
                    }           
            }
        }
      
        public override List<WebStoreAttributeValueSwatchModel> GetSwatchImages(List<AttributesSelectValuesModel> swatches, IImageHelper image)
        {

            //string _PathSmallThumbnail = image.GetImageHttpPathSmallThumbnail("");
            //string _OriginalImagepath = image.GetOriginalImagepath("");
            string _PathSmallThumbnail = image.GetImageHttpPathSmallThumbnail("") != null ? image.GetImageHttpPathSmallThumbnail("").Substring(0, image.GetImageHttpPathSmallThumbnail("").LastIndexOf('/')) : string.Empty;
            string _OriginalImagepath = image.GetOriginalImagepath("") != null ? image.GetOriginalImagepath("").Substring(0, image.GetOriginalImagepath("").LastIndexOf('/')) : string.Empty;
            List<WebStoreAttributeValueSwatchModel> list = new List<WebStoreAttributeValueSwatchModel>();
            var distinctColor=swatches.Select(x => x.Value).Distinct();
            foreach (var itemouter in distinctColor)
            {
                var item = swatches?.Where(x => x.Value == itemouter)?.FirstOrDefault();
                WebStoreAttributeValueSwatchModel model = new WebStoreAttributeValueSwatchModel
                {
                    Code = item.Code,
                    Value = item.Value,
                    ImageSmallThumbnailPath = _PathSmallThumbnail + "/" + item.Path,//Swatch Img
                    ImageSmallPath = _OriginalImagepath + "/" + item.VariantImagePath//hover change Image
                };
                list.Add(model);
            }           
            return list;
        }
        public virtual void GetpublishedConfigurableProducts(List<PublishProductModel> publishProducts, int PortalId, string attributeCode)
        {
            ImageHelper imageHelper = new ImageHelper(PortalId);

            foreach (PublishProductModel publishProduct in publishProducts)
            {
                string productType = ZnodeConstant.ConfigurableProduct;
                if (IsConfigurableProduct(publishProduct, productType))
                {
                    List<AttributesSelectValuesModel> selectValues = GetSelectValues(attributeCode, publishProduct);
                    if (selectValues?.Count > 0)
                    {
                        publishProduct.AlternateImages = GetPublishSwatchImages(selectValues, imageHelper);
                    }
                }

            }
        }
        private bool IsConfigurableProduct(PublishProductModel publishProduct, string productType)
        {
            return publishProduct.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductType)?.SelectValues.FirstOrDefault().Code == productType;
        }
        private List<AttributesSelectValuesModel> GetSelectValues(string attributeCode, PublishProductModel publishProduct)
        {
            return publishProduct.Attributes.FirstOrDefault(x => x.AttributeCode.Equals(attributeCode, StringComparison.OrdinalIgnoreCase))?.SelectValues.OrderBy(x => x.VariantDisplayOrder).DistinctBy(z => z.Code).ToList();
            
        }
        private List<ProductAlterNateImageModel> GetPublishSwatchImages(List<AttributesSelectValuesModel> swatches, ImageHelper image)
        {
            List<ProductAlterNateImageModel> list = new List<ProductAlterNateImageModel>();
            var distinctColor = swatches.Select(x => x.Value).Distinct();
            
            foreach (var itemouter in distinctColor)
            {
                var item = swatches?.Where(x => x.Value == itemouter)?.FirstOrDefault();
                ProductAlterNateImageModel model = new ProductAlterNateImageModel
                    {
                        FileName = item.Code,
                        ImageLargePath=item.Value,
                        ImageSmallThumbNail = image.GetImageHttpPathSmallThumbnail(item.Path),
                        ImageSmallPath = image.GetOriginalImagepath(item.VariantImagePath)
                    };
                    list.Add(model);
            }            
          
            return list;
        }
    }
}
