﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Services;

namespace Znode.Api.Custom.Service.IService
{
    public interface IDSPublishProductService : IPublishProductService
    {
        DataTable GetStoreLocationDetails(string state, string sku);
    }
}
