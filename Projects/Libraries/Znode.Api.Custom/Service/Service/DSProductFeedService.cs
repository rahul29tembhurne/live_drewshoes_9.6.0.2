﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Api.Custom.Service.Service
{
    public class DSProductFeedService : ProductFeedService
    {
        private readonly IZnodeRepository<ZnodeProductFeed> _productFeedRepository;
        private readonly IZnodeRepository<ZnodeProductFeedPriority> _productFeedPriorityRepository;
        private readonly IZnodeRepository<ZnodeProductFeedSiteMapType> _productFeedSiteMapTypeRepository;
        private readonly IZnodeRepository<ZnodeProductFeedTimeStamp> _productFeedTimeStampRepository;
        private readonly IZnodeRepository<ZnodeProductFeedType> _productFeedTypeRepository;
        private readonly string strNamespace = ZnodeApiSettings.SiteMapNameSpace;
        private readonly string strGoogleFeedNamespace = ZnodeApiSettings.GoogleProductFeedNameSpace;
        private const string ItemCategory = "category";
        private const string ItemContentPages = "content";
        private const string ItemProduct = "product";
        private readonly string ItemUrlset = "urlset";
        private const string ItemAll = "all";

        public DSProductFeedService() : base()
        {
            _productFeedRepository = new ZnodeRepository<ZnodeProductFeed>();
            _productFeedPriorityRepository = new ZnodeRepository<ZnodeProductFeedPriority>();
            _productFeedSiteMapTypeRepository = new ZnodeRepository<ZnodeProductFeedSiteMapType>();
            _productFeedTimeStampRepository = new ZnodeRepository<ZnodeProductFeedTimeStamp>();
            _productFeedTypeRepository = new ZnodeRepository<ZnodeProductFeedType>();
        }

        public override ProductFeedModel CreateGoogleSiteMap(ProductFeedModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            string domainName = model.SuccessXMLGenerationMessage;
            ProductFeedModel generatedXml = new ProductFeedModel();
            string feedType = string.Empty;
            bool isFeed = false;

            //check if Feed is for Site map or Product
            CheckIsSiteMapOrProduct(model, out feedType, out isFeed);

            int fileNameCount = 0;
            DSZnodeProductFeedHelper znodeProductFeed = new DSZnodeProductFeedHelper();
            string portalidSrt = string.Join(",", Array.ConvertAll(model.PortalId, x => x.ToString()));
            ZnodeLogging.LogMessage("portalidSrt:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, portalidSrt);
            if (isFeed)
                fileNameCount = znodeProductFeed.GetProductList(portalidSrt, this.strGoogleFeedNamespace, model.FileName, model.Title, model.Link, model.Description, feedType, model.LocaleId, model.ProductFeedTimeStampName, model.Date);
            else
            {
                //Call if Site map to be generated for Category/Content Page
                ZnodeLogging.LogMessage("Parameter for getting fileNameCount", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new object[] { "ProductFeedModel model", fileNameCount, znodeProductFeed, portalidSrt, feedType });
                fileNameCount = GetFileNameCount(model, fileNameCount, znodeProductFeed, portalidSrt, feedType);
                //Show error if XML for feed or site map is not generated.
                if (Equals(fileNameCount, 0))
                {
                    //generatedXml.ErrorMessage = Admin_Resources.ErrorGeneratingFeed;
                    return generatedXml;
                }
            }

            string siteMapFile = string.Empty;
            //Create XML file if count more than 0.  FileCount will only be more than 0 if XML is generated for Feed or Site Map.

            if (fileNameCount > 0)
                /*Nivi Code start: Add ProductFeedSiteMapTypeCode to check sitemap is category, product, contentpage or all*/
                //siteMapFile = znodeProductFeed.GenerateGoogleSiteMapIndexFiles(fileNameCount, model.ProductFeedSiteMapTypeCode + "##_" + model.Stores + model.FileName, Convert.ToString(model.ProductFeedPriority));
                //siteMapFile = znodeProductFeed.GenerateGoogleSiteMapIndexFiles(fileNameCount, model.ProductFeedSiteMapTypeCode + "##_" + model.FileName, Convert.ToString(model.ProductFeedPriority));
                /*Nivi Code End: Add ProductFeedSiteMapTypeCode to check sitemap is category, product, contentpage or all*/

                //changes code
                siteMapFile = znodeProductFeed.GenerateGoogleSiteMapIndexFiles(fileNameCount, model.ProductFeedSiteMapTypeCode + "##_" + portalidSrt + model.FileName, Convert.ToString(model.ProductFeedPriority));

            //File is saved properly in folder location then send that file name along with domain name to be downloaded by Admin user.
            if (!string.IsNullOrEmpty(siteMapFile))
            {
                generatedXml.SuccessXMLGenerationMessage = siteMapFile;
                model.SuccessXMLGenerationMessage = siteMapFile;
                model.SuccessXMLGenerationMessage = $"{domainName}{model.SuccessXMLGenerationMessage.Replace("~", string.Empty)}";
                model.Stores = portalidSrt;

                //Set product feed master details.
                SetProductFeedMasterDetails(model);

                if (!model.IsFromScheduler)
                    _productFeedRepository.Insert(model.ToEntity<ZnodeProductFeed>());
            }
            ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return model;
        }

        private void SetProductFeedMasterDetails(ProductFeedModel model)
        {
            ZnodeLogging.LogMessage("SetProductFeedMasterDetails method execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            GetProductFeedPriorityDetails(model);
            GetProductFeedSiteMapTypeDetails(model);
            GetProductFeedTimeStampCodeDetails(model);
            GetProductFeedTypeCodeDetails(model);
        }

        private void GetProductFeedTypeCodeDetails(ProductFeedModel model)
        {
            ZnodeLogging.LogMessage("Input Parameter ProductFeedModel having ProductFeedTypeCode", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.ProductFeedTypeCode);
            if (IsNotNull(model))
            {
                ZnodeProductFeedType productFeedType = _productFeedTypeRepository.Table.Where(w => w.ProductFeedTypeCode == model.ProductFeedTypeCode)?.FirstOrDefault();
                model.ProductFeedTypeId = IsNotNull(productFeedType) ? productFeedType.ProductFeedTypeId : 0;
            }
        }

        private void GetProductFeedTimeStampCodeDetails(ProductFeedModel model)
        {
            ZnodeLogging.LogMessage("Input Parameter ProductFeedModel having ProductFeedTimeStampName", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.ProductFeedTimeStampName);
            if (IsNotNull(model))
            {
                ZnodeProductFeedTimeStamp productFeedTimeStamp = _productFeedTimeStampRepository.Table.Where(w => w.ProductFeedTimeStampName == model.ProductFeedTimeStampName)?.FirstOrDefault();
                model.ProductFeedTimeStampId = IsNotNull(productFeedTimeStamp) ? productFeedTimeStamp.ProductFeedTimeStampId : 0;
            }
        }

        private void GetProductFeedPriorityDetails(ProductFeedModel model)
        {
            ZnodeLogging.LogMessage("Input Parameter ProductFeedModel having ProductFeedPriority", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.ProductFeedPriority);
            if (IsNotNull(model))
            {
                ZnodeProductFeedPriority productFeedPriority = _productFeedPriorityRepository.Table.Where(w => w.ProductFeedPriority == model.ProductFeedPriority)?.FirstOrDefault();
                model.ProductFeedPriorityId = IsNotNull(productFeedPriority) ? productFeedPriority.ProductFeedPriorityId : 0;
            }
        }

        private void GetProductFeedSiteMapTypeDetails(ProductFeedModel model)
        {
            ZnodeLogging.LogMessage("Input Parameter ProductFeedModel having ProductFeedSiteMapTypeCode", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.ProductFeedSiteMapTypeCode);
            if (IsNotNull(model))
            {
                ZnodeProductFeedSiteMapType productFeedSiteMapType = _productFeedSiteMapTypeRepository.Table.Where(w => w.ProductFeedSiteMapTypeCode == model.ProductFeedSiteMapTypeCode)?.FirstOrDefault();
                model.ProductFeedSiteMapTypeId = IsNotNull(productFeedSiteMapType) ? productFeedSiteMapType.ProductFeedSiteMapTypeId : 0;
            }
        }

        private int GetFileNameCount(ProductFeedModel model, int fileNameCount, ZnodeProductFeedHelper znodeProductFeed, string portalidSrt, string feedType = "")
        {
            switch (model.ProductFeedSiteMapTypeCode.ToLower())
            {
                case ItemCategory:
                    fileNameCount = znodeProductFeed.GetCategoryList(portalidSrt, model, ItemUrlset, this.strNamespace);
                    break;
                case ItemContentPages:
                    fileNameCount = znodeProductFeed.GetContentPagesList(portalidSrt, ItemUrlset, this.strNamespace, model);
                    break;
                case ItemProduct:
                    fileNameCount = znodeProductFeed.GetProductXMLList(portalidSrt, this.strNamespace, feedType, ItemUrlset, model);
                    break;
                case ItemAll:
                    fileNameCount = znodeProductFeed.GetAllList(model, portalidSrt, ItemUrlset, this.strNamespace, feedType);
                    break;
                default:
                    break;
            }
            ZnodeLogging.LogMessage("fileNameCount:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, fileNameCount);
            return fileNameCount;
        }

        private void CheckIsSiteMapOrProduct(ProductFeedModel model, out string feedType, out bool isFeed)
        {
            ZnodeLogging.LogMessage("Input Parameter ProductFeedModel having ProductFeedTypeCode", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.ProductFeedTypeCode);
            feedType = string.Empty;
            isFeed = false;
            if (!model.ProductFeedTypeCode.ToLower().Equals("xmlsitemap"))
            {
                feedType = model.ProductFeedTypeCode;
                isFeed = true;
            }
        }
    }
}
