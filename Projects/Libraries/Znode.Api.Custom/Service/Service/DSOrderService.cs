﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.ECommerce.Fulfillment;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
using Znode.Libraries.Resources;

namespace Znode.Api.Custom.Service.Service
{
    public class DSOrderService : OrderService
    {
        #region private variable
        private string url = Convert.ToString(ConfigurationManager.AppSettings["DrewEverestApiUrl"]);
        private string key = Convert.ToString(ConfigurationManager.AppSettings["DrewEverestApiKey"]);
        private int RequestTimeout { get; set; } = 10000000;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _omsOrderDetailRepository;
        private readonly IZnodeRepository<ZnodePortal> _portalRepository;
        private readonly IZnodeRepository<ZnodeUser> _portaluserRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> OmsOrderLineItem;
        #endregion
        string PortalIdDrew = ConfigurationManager.AppSettings["PortalIDDrew"];
        string PortalIdRos = ConfigurationManager.AppSettings["PortalIDRos"];
        string PortalIdBellini = ConfigurationManager.AppSettings["PortalIDBellini"];
        string PortalIDDealer = ConfigurationManager.AppSettings["PortalIDDealer"];
        #region Constructor
        public DSOrderService() : base()
        {
            _omsOrderDetailRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();

        }
        #endregion Constructor



        #region public methods

        public override OrderModel CreateOrder(ShoppingCartModel model)
        {
            model.ShoppingCartItems.ForEach(y =>y.Custom1 = y.ProductCode );
            OrderModel orderModel = base.CreateOrder(model);
            /*
             * EVEREST CREATE ORDER SECTION BEGINS
             1.  Call Address Creation API and pass AddressModel to it get generated ShipToCode from it.
             2.  Call Order Creation API and pass Ordermodel to it and get generated Everest Order Number from it.
             3. If order Success Save EverestOrderNumber in some field
             4. If fail then?
             */
            try
            {
                /*
                Dealer Login Provide User Basis ContactCode and CustomerCode
                User Basis ContactCode 
                User Basis CustomerCode 
                */
                if(PortalId == Convert.ToInt32(PortalIDDealer))
                {               
                ZnodeUser user = new ZnodeRepository<ZnodeUser>().Table.Where(x => x.UserId == model.UserId).FirstOrDefault();               
                orderModel.Custom3 = user.ExternalId;             
                orderModel.Custom4 = user.Custom1;
                orderModel.ShippingAddress.ExternalId = user.ExternalId;
                }
                orderModel.AdditionalInstructions = model.AdditionalInstructions;
                orderModel.ShippingId = model.ShippingId;
                if (model.ShippingAddress.EmailAddress == "" || model.ShippingAddress.EmailAddress == null)
                    model.ShippingAddress.EmailAddress = orderModel.ShippingAddress.EmailAddress;
                string ShipToCode = CreateAddressInEverest(model.ShippingAddress, orderModel.OrderNumber);
                if (ShipToCode != string.Empty)
                {
                    // Log("ShipToCode=" + ShipToCode);
                    model.ShoppingCartItems.ForEach(y =>
                    orderModel.OrderLineItems.Where(x => x.Sku == y.ConfigurableProductSKUs).FirstOrDefault().Custom1 = y.ProductCode
                    );
                                    
                    if (orderModel.CouponCode == null)
                    {
                        orderModel.CouponCode = model.Shipping.ShippingDiscountDescription;
                    }
                    if (model.Coupons?.Count > 0)
                    {
                        orderModel.CouponCode = model.Coupons[0].Code;
     
                    }
                    orderModel.Custom2 = ShipToCode;
                    int everestOrderNo = CreateOrderInEverest(orderModel);
                    // Log("everestOrderNo=" + Convert.ToString(everestOrderNo));
                    /*Everest Order Number need to be saved in our system if Custom1 is being used for some other
                     Purpose then set everestOrderNo to some other field */
                    ZnodeOmsOrderDetail createdOrder = new ZnodeRepository<ZnodeOmsOrderDetail>().Table.Where(x => x.OmsOrderId == orderModel.OmsOrderId).FirstOrDefault();
                    //createdOrder.Custom1 = Convert.ToString(everestOrderNo);
                    createdOrder.ExternalId = Convert.ToString(everestOrderNo);
                    createdOrder.Custom2 = ShipToCode;
                   if (everestOrderNo == 0)
                    {
                        createdOrder.OmsOrderStateId = 50;//PENDING APPROVAL                                
                    }
                    //PENDING APPROVAL                      
                    _omsOrderDetailRepository.Update(createdOrder);
                    ZnodeLogging.LogMessage("Create Order Method ShipToCode=: " + ShipToCode + " everestOrderNo: " + Convert.ToString(everestOrderNo) + "ZnodeOrder Number: " + orderModel.OrderNumber, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                    //  Log("Order Updated");
                }
                else
                {
                    ZnodeLogging.LogMessage("Unable to Create Order In Everest ErrorDetails as address creation failed", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                    ZnodeOmsOrderDetail createdOrder = new ZnodeRepository<ZnodeOmsOrderDetail>().Table.Where(x => x.OmsOrderId == orderModel.OmsOrderId).FirstOrDefault();
                    createdOrder.OmsOrderStateId = 50;//PENDING APPROVAL
                    _omsOrderDetailRepository.Update(createdOrder);
                    
                }
            }
            catch (Exception ex)
            {
                Log("ERROR!!-" + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace);
                ZnodeLogging.LogMessage("Unable to Create Order In Everest ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                ZnodeOmsOrderDetail createdOrder = new ZnodeRepository<ZnodeOmsOrderDetail>().Table.Where(x => x.OmsOrderId == orderModel.OmsOrderId).FirstOrDefault();
                createdOrder.OmsOrderStateId = 50;//PENDING APPROVAL
               _omsOrderDetailRepository.Update(createdOrder);
                              
            }
            return orderModel;
        }

        public override OrderModel UpdateOrder(OrderModel model)
        {
            string Orderstatus = model.OrderState;
            OrderModel orderModel = base.UpdateOrder(model);            
            try
            {
                if (Orderstatus == "Pending Approval")
                {
                    /*
                    Dealer Login Provide User Basis ContactCode and CustomerCode
                    User Basis ContactCode 
                    User Basis CustomerCode 
                    */
                    if (PortalId == Convert.ToInt32(PortalIDDealer))
                    {
                        ZnodeUser user = new ZnodeRepository<ZnodeUser>().Table.Where(x => x.UserId == model.UserId).FirstOrDefault();
                        orderModel.Custom3 = user.ExternalId;
                        orderModel.Custom4 = user.Custom1;
                        orderModel.ShippingAddress.ExternalId = user.ExternalId;
                    }

                    //Shipping Address Statecode
                    orderModel.ShippingAddress.StateCode = orderModel.ShippingAddress.StateName;

                    orderModel.AdditionalInstructions = model.AdditionalInstructions;
                    orderModel.ShippingId = model.ShippingId;
                    if (model.ShippingAddress.EmailAddress == "" || model.ShippingAddress.EmailAddress == null)
                        model.ShippingAddress.EmailAddress = orderModel.ShippingAddress.EmailAddress;
                    string ShipToCode = CreateAddressInEverest(model.ShippingAddress, orderModel.OrderNumber);
                    if (ShipToCode != string.Empty)
                    {
                        //Discount Amount                  
                        foreach (var item in orderModel.OrderLineItems)
                        {
                            if (item.ParentOmsOrderLineItemsId != null)
                            {
                                ZnodeOmsOrderLineItem orderLineItem = new ZnodeRepository<ZnodeOmsOrderLineItem>().Table.Where(x => x.OmsOrderLineItemsId == item.ParentOmsOrderLineItemsId).FirstOrDefault();
                                orderModel.OrderLineItems.Where(x => x.ParentOmsOrderLineItemsId == item.ParentOmsOrderLineItemsId).FirstOrDefault().DiscountAmount = orderLineItem.DiscountAmount;
                            }
                        }

                        //Remove OrderLine Items Configurable
                        orderModel.OrderLineItems = orderModel.OrderLineItems.Where(x => x.ParentOmsOrderLineItemsId != null).ToList();

                        //Custom1 for Product Code
                        model.ShoppingCartModel.ShoppingCartItems.ForEach(y =>
                        orderModel.OrderLineItems.Where(x => x.Sku == y.ConfigurableProductSKUs).FirstOrDefault().Custom1 = y.Custom1
                        );
                        if (orderModel.CouponCode == null)
                        {
                            orderModel.CouponCode = model.CouponCode;
                        }
                        if (orderModel.ShoppingCartModel.Coupons?.Count > 0)
                        {
                            orderModel.CouponCode = orderModel.ShoppingCartModel.Coupons[0].Code;
                        }
                        orderModel.Custom2 = ShipToCode;
                        int everestOrderNo = CreateOrderInEverest(orderModel);
                        // Log("everestOrderNo=" + Convert.ToString(everestOrderNo));
                        /*Everest Order Number need to be saved in our system if Custom1 is being used for some other
                         Purpose then set everestOrderNo to some other field */
                        ZnodeOmsOrderDetail createdOrder = new ZnodeRepository<ZnodeOmsOrderDetail>().Table.Where(x => x.OmsOrderId == orderModel.OmsOrderId).FirstOrDefault();
                        //createdOrder.Custom1 = Convert.ToString(everestOrderNo);
                        createdOrder.ExternalId = Convert.ToString(everestOrderNo);
                        createdOrder.Custom2 = ShipToCode;
                        if (everestOrderNo == 0)
                        {
                            createdOrder.OmsOrderStateId = 50;//PENDING APPROVAL                                
                        }
                        else
                        {
                            createdOrder.OmsOrderStateId = 130;//ORDER RECEIVED       
                            _omsOrderDetailRepository.Update(createdOrder);
                            ZnodeLogging.LogMessage("Create Order Method ShipToCode=: " + ShipToCode + " everestOrderNo: " + Convert.ToString(everestOrderNo) + "ZnodeOrder Number: " + orderModel.OrderNumber, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                            //  Log("Order Updated");
                        }
                    }
                    else
                    {
                        ZnodeLogging.LogMessage("Unable to Create Order In Everest ErrorDetails as address creation failed", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                        ZnodeOmsOrderDetail createdOrder = new ZnodeRepository<ZnodeOmsOrderDetail>().Table.Where(x => x.OmsOrderId == orderModel.OmsOrderId).FirstOrDefault();
                        createdOrder.OmsOrderStateId = 50;//PENDING APPROVAL
                        _omsOrderDetailRepository.Update(createdOrder);
                    }
                }
            }
            catch (Exception ex)
            {
                Log("ERROR!!-" + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace);
                ZnodeLogging.LogMessage("Unable to Create Order In Everest ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                ZnodeOmsOrderDetail createdOrder = new ZnodeRepository<ZnodeOmsOrderDetail>().Table.Where(x => x.OmsOrderId == orderModel.OmsOrderId).FirstOrDefault();
                createdOrder.OmsOrderStateId = 50;//PENDING APPROVAL
                _omsOrderDetailRepository.Update(createdOrder);                
            }
            
            return orderModel;
        }

        //public override OrderModel UpdateOrder(OrderModel model)
        //{

        //    OrderModel modifiedModel = base.UpdateOrder(model);
        //    try
        //    {
        //        bool isOrderUpdatedInEverest = false;
        //        ZnodeLogging.LogMessage("UPDATEORDER: model.OrderState=" + model.OrderState, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
        //        if (model.OrderState.ToUpper() == "CANCELLED")
        //        {
        //            isOrderUpdatedInEverest = CancelOrderinEverest(model);
        //        }
        //        else
        //        {
        //            isOrderUpdatedInEverest = UpdateOrderinEverest(model);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ZnodeLogging.LogMessage("Unable to Update Order In Everest ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
        //    }

        //    return modifiedModel;
        //}
        #endregion

        #region Everest
        #region Address Creation    
        private string CreateAddressInEverest(AddressModel address,string ordernumber)
        {
            string ShipToCode = "";
            try
            {
                string addressurl = url + "CreateAddress";
                //string json = JsonConvert.SerializeObject(address);
                if (address.CountryName == "US")
                    address.CountryName = "United States";
                string result = GetEverestResponse(addressurl, key, JsonConvert.SerializeObject(address));


                if (result.Contains("Error"))
                {
                    ZnodeLogging.LogMessage("Unable to Create Address In Everest ErrorDetails:" + result, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                    string Tomail = ConfigurationManager.AppSettings["ToMail"].ToString();
                    ZnodeEmail.SendEmail(Tomail, ZnodeConfigManager.SiteConfig.AdminEmail, "", "Unable to Create Order In Everest", "Everest Order Failed Order Number:" + ordernumber + ""+  result  + "Logging Component :" + ZnodeLogging.Components.OMS.ToString() + "" + "Trace Error :" + TraceLevel.Error, true, "", Tomail);
                }
                else
                {
                    string firstString = "<AddressCode>";
                    string lastString = "</AddressCode>";
                    int pos1 = result.IndexOf(firstString) + firstString.Length;
                    int pos2 = result.Substring(pos1).IndexOf(lastString);
                    ShipToCode = result.Substring(pos1, pos2);

                }
                return ShipToCode;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Unable to Create Address In Everest ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                string Tomail = ConfigurationManager.AppSettings["ToMail"].ToString();
                ZnodeEmail.SendEmail(Tomail, ZnodeConfigManager.SiteConfig.AdminEmail, "", "Unable to Create Order In Everest", "Everest Order Failed Order Number:" + ordernumber + "<br>" + "Everest ErrorDetails :" + ex.Message + "<br>" + "Inner Exception: :" + ex.InnerException + "<br>" + " StackTrace: " + ex.StackTrace, true, "", Tomail);
                return ShipToCode;
            }
        }
        #endregion

        #region Order Section
        private int CreateOrderInEverest(OrderModel model)
        {
            int everestOrderNumber = 0;
            try
            {
                string orderCreationurl = url + "/CreateSaleOrder";
                // string json = JsonConvert.SerializeObject(model);
                string result = GetEverestResponse(orderCreationurl, key, JsonConvert.SerializeObject(model));
                if (result.Contains("Error"))
                {
                    ZnodeLogging.LogMessage("Unable to Create Order In Everest ErrorDetails:" + result, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                    string Tomail = ConfigurationManager.AppSettings["ToMail"].ToString();
                    ZnodeEmail.SendEmail(Tomail, ZnodeConfigManager.SiteConfig.AdminEmail, "", "Unable to Create Order In Everest", "Everest Order Failed Order Number:" + model.OrderNumber + "<br>" + result + "Logging Component :" + ZnodeLogging.Components.OMS.ToString() + "\n<br>" + "Trace Error :" + TraceLevel.Error, true, "", Tomail);
                }
                else
                {
                    string firstString = "<DocumentNo>";
                    string lastString = "</DocumentNo>";
                    int pos1 = result.IndexOf(firstString) + firstString.Length;
                    int pos2 = result.Substring(pos1).IndexOf(lastString);

                    string shipviacodestart = "<ShipViaCode>";
                    string shipviacodeend = "</ShipViaCode>";
                    int ps1 = result.IndexOf(shipviacodestart) + shipviacodestart.Length;
                    int ps2 = result.Substring(ps1).IndexOf(shipviacodeend);
                    string shipviacode = result.Substring(ps1, ps2);
                    ZnodeLogging.LogMessage("OrderNo = " + model.OrderNumber + " SHIPVIACODE = " + shipviacode, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);

                    everestOrderNumber = Convert.ToInt32(result.Substring(pos1, pos2));

                }

                return everestOrderNumber;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Unable to Create Order In Everest ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                string Tomail = ConfigurationManager.AppSettings["ToMail"].ToString();
                ZnodeEmail.SendEmail(Tomail, ZnodeConfigManager.SiteConfig.AdminEmail, "", "Unable to Create Order In Everest", "Everest Order Failed Order Number:" + model.OrderNumber + "<br>" + "Everest ErrorDetails :" + ex.Message + "<br>" + "Inner Exception: :" + ex.InnerException + "<br>" + " StackTrace: " + ex.StackTrace, true, "", Tomail);
                return everestOrderNumber;
            }

        }

        private bool UpdateOrderinEverest(OrderModel model)
        {
            bool issuccess = false;
            try
            {
                string orderCreationurl = url + "/UpdateSaleOrder";
                // string json = JsonConvert.SerializeObject(model);
                string result = GetEverestResponse(orderCreationurl, key, JsonConvert.SerializeObject(model));

                if (result.Contains("Error"))
                {
                    ZnodeLogging.LogMessage("Unable to Update Order In Everest ErrorDetails:" + result, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                }
                else
                {
                    issuccess = true;
                }
                return issuccess;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Unable to Update Order In Everest ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return issuccess;
            }

        }

        private bool CancelOrderinEverest(OrderModel model)
        {
            bool issuccess = false;
            try
            {
                string orderCreationurl = url + "/CancelSaleOrder";
                // string json = JsonConvert.SerializeObject(model);
                string result = GetEverestResponse(orderCreationurl, key, JsonConvert.SerializeObject(model));

                if (result.Contains("Error"))
                {
                    ZnodeLogging.LogMessage("Unable to Cancel Order In Everest ErrorDetails:" + result, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                }
                else
                {
                    issuccess = true;
                }
                return issuccess;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Unable to Cancel Order In Everest ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return issuccess;
            }

        }
        #endregion

        #region response      

        private string GetEverestResponse(string url, string key, string data)
        {

            byte[] dataBytes = Encoding.UTF8.GetBytes(data);

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.KeepAlive = false; // Prevents "server committed a protocol violation" error
            req.Method = "POST";
            req.ContentType = "application/json";
            req.ContentLength = dataBytes.Length;
            req.Timeout = RequestTimeout;

            string authoriseHeader = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(url + "|" + key));
            req.Headers.Add(HttpRequestHeader.Authorization, "Basic " + authoriseHeader);

            using (Stream reqStream = req.GetRequestStream())
            {
                reqStream.Write(dataBytes, 0, dataBytes.Length);
            }
            HttpWebResponse response = (HttpWebResponse)req.GetResponse();
            string result = "";
            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");

            using (var webResponse = req.GetResponse())
            {
                Stream responseStream = webResponse.GetResponseStream();
                StreamReader readStream = new StreamReader(responseStream, encode);
                Char[] read = new Char[256];

                int count = readStream.Read(read, 0, 256);
                while (count > 0)
                {

                    String str = new String(read, 0, count);
                    result = result + str;
                    count = readStream.Read(read, 0, 256);
                }
            }

            response.Close();
            return result;
        }
        #endregion

        public string Log(string logMessage)
        {
            string logfilepath = ConfigurationManager.AppSettings["FIlelogPath"];
            try
            {
                using (StreamWriter writer = File.AppendText(logfilepath + "\\" + "log.txt"))
                {
                    // Log(logMessage, w);

                    writer.Write("\r\nLog Entry : ");
                    writer.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                    writer.WriteLine("  :");
                    writer.WriteLine("  :{0}", logMessage);
                    writer.WriteLine("-------------------------------");
                }
            }
            catch (Exception ex)
            {
                return "Error : " + ex.Message + " StackTrace: " + ex.StackTrace;
            }
            return string.Empty;
        }

        //to generate order receipt
  
        public override void SendOrderStatusEmail(OrderModel orderModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            foreach (OrderLineItemModel item in orderModel.OrderLineItems)
                item.OrderLineItemCollection.AddRange(orderModel.OrderLineItems.Where(x => x.ParentOmsOrderLineItemsId == item.OmsOrderLineItemsId && x.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.AddOns)?.ToList());

            orderModel.OrderLineItems.RemoveAll(x => x.ParentOmsOrderLineItemsId == null);

            string subject = string.Empty;
            bool isEnableBcc = false;
            //if (orderModel.OrderState == ZnodeOrderStatusEnum.CANCELLED.ToString())

            if (orderModel.OrderState.ToUpper() == ZnodeOrderStatusEnum.CANCELLED.ToString())
            {
                subject = $"{"Cancelled Order Receipt"} - {orderModel.OrderNumber}";
                orderModel.ReceiptHtml = GetCancelledOrderReceiptForEmail(orderModel, out isEnableBcc);
            }
            else
            {
                // And finally attach the receipt HTML to the order and return
                subject = $"{""} - {orderModel.OrderNumber}";
                orderModel.ReceiptHtml = GetShippingReceiptForEmail(orderModel, out isEnableBcc);
            }
            if (!string.IsNullOrEmpty(orderModel.ReceiptHtml))
                SendOrderReceipt(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, subject, ZnodeConfigManager.SiteConfig.AdminEmail, string.Empty, orderModel.ReceiptHtml, isEnableBcc);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //to generate shipping status receipt
        
       
        #endregion
    }
}
