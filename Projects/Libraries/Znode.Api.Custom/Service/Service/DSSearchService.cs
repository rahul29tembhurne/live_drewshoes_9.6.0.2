﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Observer;

namespace Znode.Api.Custom.Service.Service
{
    public class DSSearchService : SearchService
    {
        #region private variable
        //private readonly IMongoRepository<ProductEntity> _ProductMongoRepository;
        #endregion
        string PortalIdDrew = ConfigurationManager.AppSettings["PortalIDDrew"];
        string PortalIdRos = ConfigurationManager.AppSettings["PortalIDRos"];
       
        #region public methods

        public override KeywordSearchModel FullTextSearch(SearchRequestModel model, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            KeywordSearchModel searchResult = new KeywordSearchModel();
            try
            {
                if (model.RefineBy.Count > 0)
                {
                    model.PageSize = 64;
                }
                int portalId; int catalogId;
                int localeId;
                //List<string> skus = new List<string>();
                FilterCollection filter = new FilterCollection();
                GetParametersValueForFilters(filters, out portalId, out catalogId, out localeId);
                expands.Remove(ZnodeConstant.Pricing);
                string valuesizelist = "";
                string valuewidthlist = "";
                string csvSKU = "";
                sorts.Add("DisplayOrder", "-1");
                //ZnodeLogging.LogMessage("Base FullTextSearch Method Started=:" + filters.Count(), ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
                searchResult = base.FullTextSearch(model, expands, filters, sorts, page);
               // ZnodeLogging.LogMessage("Base FullTextSearch Method Ended=:" + searchResult.TotalProductCount + " Product Count: " + searchResult.Products.Count, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
                try
                {
                    ///Nivi Code stock configurable products:Start
                    ///Written to Remove out of stock configurabl  if (searchResult.Products.Count != null)
                    if (searchResult?.Products != null && searchResult?.Products?.Count() > 0)
                {
                    List<List<string>> drewsize = model?.RefineBy?.Where(x => x.Key == "drewsize").Select(x => x.Value).ToList();
                    List<List<string>> drewwidth = model?.RefineBy?.Where(x => x.Key == "drewwidth").Select(x => x.Value).ToList();
                    drewsize?.ForEach(x => x.ForEach(y => valuesizelist = valuesizelist + "," + y));
                    drewwidth?.ForEach(x => x.ForEach(y => valuewidthlist = valuewidthlist + "," + y));
                    //List<string> skus = new List<string>();
                    List<string> skus = searchResult?.Products?.Select(x => x.SKU).ToList();
                    skus?.ForEach(x => csvSKU = csvSKU + "," + x);
                    List<string> skumodel = GetPLPInventoryBySKUs(skus, portalId, valuesizelist, valuewidthlist).ToList();
                    if (!string.IsNullOrEmpty(valuesizelist) || !string.IsNullOrEmpty(valuewidthlist))
                    {
                        if (skumodel.Count > 0)
                        {
                            foreach (string item in skumodel)
                            {
                                searchResult?.Products?.RemoveAll(y => y.SKU == item);
                            }
                        }
                    }
                }
                  
                    ///Nivi Code stock configurable products:End
                if (searchResult.Products != null)
                {
                    //ZnodeLogging.LogMessage("AttributeSwatchHelper Method Started=:" + searchResult.Products, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
                    DSAttributeSwatchHelper attributeSwatchHelper = new DSAttributeSwatchHelper();
                      if(portalId == Convert.ToInt32(PortalIdDrew))
                      {
                            attributeSwatchHelper.GetAssociatedConfigurableProducts(searchResult, model, "drewcolor");
                      }
                      else if(portalId == Convert.ToInt32(PortalIdRos))
                      {
                            attributeSwatchHelper.GetAssociatedConfigurableProducts(searchResult, model, "roscolor");
                      }
                      else
                      {
                            attributeSwatchHelper.GetAssociatedConfigurableProducts(searchResult, model, "drewcolor");
                      }                    
                    //ZnodeLogging.LogMessage("AttributeSwatchHelper Method Ended=:" + searchResult.Products.Count(), ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
                }

                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage("Unable to FullTextSearch ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Out Of the catch ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
            }
            return searchResult;
        }

        public virtual List<InventorySKUModel> GetInventoryBySKUs(IEnumerable<string> skus, int portalId)
        {
            //Znode code to check ZnodeInventory table for available inventories.
            IZnodeViewRepository<InventorySKUModel> skuInventory = new ZnodeViewRepository<InventorySKUModel>();
            skuInventory.SetParameter("@SKUs", string.Join(",", skus), ParameterDirection.Input, DbType.String);
            skuInventory.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            List<InventorySKUModel> model = skuInventory.ExecuteStoredProcedureList("Znode_GetInventoryBySkus @SKUs,@PortalId")?.ToList();
            if (model.Count() == 1)
            {
                ERPInitializer<InventorySKUModel> _erpInc = new ERPInitializer<InventorySKUModel>(new InventorySKUModel() { SKU = model.FirstOrDefault()?.SKU }, "Inventory");
                List<InventorySKUModel> inventoryModel = (List<InventorySKUModel>)_erpInc.Result;
                if (HelperUtility.IsNotNull(inventoryModel))
                    model.FirstOrDefault().Quantity = inventoryModel.FirstOrDefault().Quantity;
            }
            //Inventory realtime call
            return RealTimeProductInventoryCall(model, skus, portalId);
        }

        public virtual List<string> GetPLPInventoryBySKUs(IEnumerable<string> skus, int portalId, string valuesizelist, string valuewidthlist)
        {
            //Znode code to check ZnodeInventory table for available inventories.
            IZnodeViewRepository<string> skuInventory = new ZnodeViewRepository<string>();
            skuInventory.SetParameter("@SKUs", string.Join(",", skus), ParameterDirection.Input, DbType.String);
            skuInventory.SetParameter("@valuesizelist", valuesizelist, ParameterDirection.Input, DbType.String);
            skuInventory.SetParameter("@valuewidthlist", valuewidthlist, ParameterDirection.Input, DbType.String);
            skuInventory.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            return skuInventory.ExecuteStoredProcedureList("Nivi_GetInventoryBySkus @SKUs,@valuesizelist,@valuewidthlist,@PortalId").ToList();
        }

        #endregion

        #region private methods

        private static void GetParametersValueForFilters(FilterCollection filters, out int portalId, out int catalogId, out int localeId)
        {
            int.TryParse(filters.Where(x => x.FilterName.Equals(FilterKeys.PortalId, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()?.FilterValue, out portalId);
            int.TryParse(filters.Where(x => x.FilterName.Equals(WebStoreEnum.ZnodeCatalogId.ToString(), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()?.FilterValue, out catalogId);
            int.TryParse(filters.Where(x => x.FilterName.Equals(WebStoreEnum.LocaleId.ToString(), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()?.FilterValue, out localeId);
        }

        private List<InventorySKUModel> RealTimeProductInventoryCall(List<InventorySKUModel> znodeInventoryModel, IEnumerable<string> realtimeProductSkus, int portalId)
        {
            if (znodeInventoryModel.Count == 0)
                znodeInventoryModel = new List<InventorySKUModel>();
            //Pass decorated InventoryModel if znodeInventoryModel is empty.
            //Generate list of product inventory.
            List<InventorySKUModel> realtimeProductInventoryModel = znodeInventoryModel.Any() ? znodeInventoryModel
                : realtimeProductSkus.Select(sku => new InventorySKUModel
                {
                    SKU = sku,
                    PortalId = portalId
                }).ToList();

            //Real time inventory call
            ERPInitializer<List<InventorySKUModel>> _erpInc = new ERPInitializer<List<InventorySKUModel>>(realtimeProductInventoryModel, "ProductInventory");
            List<InventorySKUModel> inventoryModel = (List<InventorySKUModel>)_erpInc.Result;//List of child product inventory 

            //Update quantity available in returning object by the inventory received from real-time call.
            foreach (string sku in realtimeProductSkus)
            {
                if (HelperUtility.IsNotNull(inventoryModel))
                {
                    //Real-time inventory model
                    InventorySKUModel internalModel = inventoryModel.Where(x => x.SKU == sku).Select(x => x)?.FirstOrDefault();
                    //Znode inventory model
                    InventorySKUModel updateModel = znodeInventoryModel.Where(x => x.SKU == sku).Select(x => x)?.FirstOrDefault();

                    if (HelperUtility.IsNotNull(updateModel) && HelperUtility.IsNotNull(internalModel) && CheckIfSKUAvailable(znodeInventoryModel, inventoryModel))
                        updateModel.Quantity = Convert.ToDecimal(inventoryModel?.FirstOrDefault()?.Quantity);
                    else if (HelperUtility.IsNotNull(internalModel))
                        znodeInventoryModel.Add(new InventorySKUModel
                        {
                            SKU = sku,
                            Quantity = HelperUtility.IsNotNull(internalModel) ? internalModel.Quantity : 0M,
                            ReOrderLevel = HelperUtility.IsNotNull(internalModel) ? internalModel.ReOrderLevel : 0M,
                            PortalId = portalId
                        });
                }
            }
            return znodeInventoryModel;
        }

        private bool CheckIfSKUAvailable(List<InventorySKUModel> model, List<InventorySKUModel> inventoryModel)
        {
            bool isAvailable = false;
            if (HelperUtility.IsNotNull(model) || HelperUtility.IsNotNull(inventoryModel))
                isAvailable = false;
            foreach (InventorySKUModel invModel in inventoryModel)
            {
                InventorySKUModel nwModel = model.Where(x => x.SKU == invModel.SKU).Select(x => x)?.FirstOrDefault();
                if (HelperUtility.IsNotNull(nwModel) && nwModel.SKU.Equals(invModel.SKU))
                {
                    isAvailable = true;
                    break;
                }
                else
                    isAvailable = false;
            }
            return isAvailable;
        }

        #endregion
    }
}
