﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
namespace Znode.WebStore.Custom.Agents.Agents
{
   public class DSCheckoutAgentcs : CheckoutAgent
    {
        
        protected readonly IShippingClient _shippingsClient;
        protected readonly IPaymentClient _paymentClient;
        protected readonly IPortalProfileClient _profileClient;
        protected readonly ICustomerClient _customerClient;
        protected readonly IUserClient _userClient;
        protected readonly IOrderClient _orderClient;
        protected readonly ICartAgent _cartAgent;
        protected readonly IUserAgent _userAgent;
        protected readonly IPaymentAgent _paymentAgent;
        protected readonly IAccountClient _accountClient;
        protected readonly IWebStoreUserClient _webStoreAccountClient;
        protected readonly IPortalClient _portalClient;
        protected readonly IShoppingCartClient _shoppingCartClient;
        protected readonly IAddressAgent _addressAgent;
       

        public DSCheckoutAgentcs(IShippingClient shippingsClient, IPaymentClient paymentClient, IPortalProfileClient profileClient, ICustomerClient customerClient, IUserClient userClient, IOrderClient orderClient, IAccountClient accountClient, IWebStoreUserClient webStoreAccountClient, IPortalClient portalClient, IShoppingCartClient shoppingCartClient, IAddressClient addressClient)
           :base( shippingsClient,  paymentClient,  profileClient,  customerClient,  userClient,  orderClient,  accountClient,  webStoreAccountClient,  portalClient,  shoppingCartClient,  addressClient)
        {
            _shippingsClient = GetClient<IShippingClient>(shippingsClient);
            _paymentClient = GetClient<IPaymentClient>(paymentClient);
            _profileClient = GetClient<IPortalProfileClient>(profileClient);
            _customerClient = GetClient<ICustomerClient>(customerClient);
            _userClient = GetClient<IUserClient>(userClient);
            _orderClient = GetClient<IOrderClient>(orderClient);
            _accountClient = GetClient<IAccountClient>(accountClient);
            _webStoreAccountClient = GetClient<IWebStoreUserClient>(webStoreAccountClient);
            _portalClient = GetClient<IPortalClient>(portalClient);
            _shoppingCartClient = GetClient<IShoppingCartClient>(shoppingCartClient);
            _userAgent = GetService<IUserAgent>();
            _cartAgent = GetService<ICartAgent>();
            _paymentAgent = GetService<IPaymentAgent>();
            _addressAgent = GetService<IAddressAgent>();
        }
        //Returns true if the passed value is not null, else return false.
        public static bool IsNotNull(object value)
            => !Equals(value, null);
        //Returns true if the passed value is null else false.
        public static bool IsNull(object value)
            => Equals(value, null);

        public override OrdersViewModel SubmitOrder(SubmitOrderViewModel submitOrderViewModel)
        {
            if (IsNotNull(submitOrderViewModel))
            {
                //Get cart from session or by cookie.
                ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                          _cartAgent.GetCartFromCookie();

                if (IsNotNull(cartModel))
                {
                    UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

                    //Set IsQuoteOrder true if quote id is greater than zero or user permission access is does not require approver.
                    if (IsNotNull(userViewModel))
                    {
                        userViewModel.CreatedDate = string.Empty;
                        SetIsQuoteOrder(cartModel, userViewModel);
                        string message = string.Empty;
                        if (!_userAgent.ValidateUserBudget(out message))
                        {
                            return (OrdersViewModel)GetViewModelWithErrorMessage(new OrdersViewModel(), message);
                        }
                        UpdateUserDetailsInSession(cartModel, userViewModel);
                    }

                    RemoveInvalidDiscountCode(cartModel);

                    UserViewModel user = (IsNotNull(userViewModel) && userViewModel.UserId > 0) ? userViewModel : GetFromSession<UserViewModel>(WebStoreConstants.GuestUserKey);

                    //Get the payment details.
                    GetPaymentDetails(submitOrderViewModel.PaymentSettingId, cartModel);

                    //Check if billing address of cart model is not null then create it billing address of guest user.
                    if (IsNotNull(cartModel.BillingAddress) && !Convert.ToBoolean(cartModel.ShippingAddress.IsDefaultBilling))
                    {
                        if (cartModel.Payment?.PaymentSetting?.IsBillingAddressOptional == true)
                        {
                            cartModel.BillingAddress = cartModel.ShippingAddress;
                            cartModel.BillingAddress.AddressId = 0;
                            cartModel.BillingAddress.IsBilling = true;
                            cartModel.BillingAddress.IsDefaultBilling = true;
                        }
                    }

                    cartModel.OrderNumber = !string.IsNullOrEmpty(submitOrderViewModel.OrderNumber) ? submitOrderViewModel.OrderNumber
                                            : GenerateOrderNumber(cartModel.PortalId);

                    if (IsNull(user) || user?.UserId < 1)
                    {
                        if (IsAmazonPayEnable(submitOrderViewModel))
                        {
                            SetUsersPaymentDetails(submitOrderViewModel.PaymentSettingId, cartModel, true);
                            SetAmazonAddress(submitOrderViewModel, cartModel);
                            user = CreateAnonymousUserAccount(cartModel.BillingAddress, cartModel.ShippingAddress?.EmailAddress);
                            UserViewModel oldSession = GetFromSession<UserViewModel>(WebStoreConstants.GuestUserKey);
                            if (!Equals(oldSession, null))
                            {
                                oldSession.GuestUserId = oldSession.UserId;
                                if (IsNotNull(userViewModel))
                                    userViewModel.UserId = oldSession.UserId;
                                SaveInSession(WebStoreConstants.GuestUserKey, oldSession);
                            }
                        }
                        else
                        {
                            user = CreateAnonymousUserAccount(cartModel.BillingAddress, cartModel.ShippingAddress?.EmailAddress);
                        }
                    }

                    //Get the list of all addresses associated to current logged in user.
                    List<AddressModel> userAddresses = GetUserAddressList();

                    if (IsNull(userAddresses) || userAddresses.Count < 1)
                    {
                        if (IsAmazonPayEnable(submitOrderViewModel) && !Equals(cartModel.ShippingAddress, null) && (string.IsNullOrEmpty(cartModel.ShippingAddress.Address1) || string.IsNullOrEmpty(cartModel.ShippingAddress.FirstName)))
                        {
                            SetUsersPaymentDetails(submitOrderViewModel.PaymentSettingId, cartModel, true);
                            SetAmazonAddress(submitOrderViewModel, cartModel);
                            cartModel.ShippingAddress.IsDefaultBilling = true;
                            cartModel.ShippingAddress.IsDefaultShipping = true;
                        }
                        if (!string.IsNullOrEmpty(cartModel?.Payment?.PaymentName) && Equals(cartModel.Payment.PaymentName.Replace("_", "").ToLower(), ZnodeConstant.PayPalExpress.ToLower()))
                        {
                            if (string.IsNullOrEmpty(submitOrderViewModel.PayPalToken))
                            {
                                userAddresses = GetAnonymousUserAddresses(cartModel, submitOrderViewModel);
                            }
                        }
                        else if (IsNotNull(submitOrderViewModel?.PaymentType) && Equals(submitOrderViewModel?.PaymentType.ToLower(), ZnodeConstant.AmazonPay.ToLower()))
                        {
                            SetAmazonAddress(submitOrderViewModel, cartModel);
                            SetUserDetails(user, cartModel);
                            cartModel.ShippingAddress.IsDefaultBilling = true;
                            cartModel.ShippingAddress.IsDefaultShipping = true;
                            userAddresses = GetAnonymousUserAddresses(cartModel, submitOrderViewModel);
                        }
                        else
                        {
                            userAddresses = GetAnonymousUserAddresses(cartModel, submitOrderViewModel);
                        }
                    }
                    submitOrderViewModel.UserId = user.UserId;

                    //Send shipping address in cart for validation, 
                    //if it is not available then only send shipping address from user address list for validation in USPS.
                    BooleanModel booleanModel;
                    if (IsNotNull(submitOrderViewModel?.PaymentType) && Equals(submitOrderViewModel?.PaymentType.ToLower(), ZnodeConstant.AmazonPay.ToLower()) && !submitOrderViewModel.IsFromAmazonPay)
                    {
                        booleanModel = new BooleanModel { IsSuccess = true };
                    }
                    else { booleanModel = IsValidAddressForCheckout((IsNull(cartModel?.ShippingAddress) || cartModel?.ShippingAddress?.AddressId == 0) ? userAddresses?.Where(x => x.AddressId == submitOrderViewModel.ShippingAddressId)?.FirstOrDefault() : cartModel?.ShippingAddress); }
                    //Check whether address is valid or not.


                     //Nivi status change for address validation//
                      booleanModel.IsSuccess = true;
                    if ((!booleanModel.IsSuccess) &&
                        !(bool)PortalAgent.CurrentPortal.PortalFeatureValues.Where(x => x.Key.Contains(StoreFeature.Require_Validated_Address.ToString()))?.FirstOrDefault().Value)
                    {
                        return (OrdersViewModel)GetViewModelWithErrorMessage(new OrdersViewModel(), booleanModel.ErrorMessage ?? "AddressValidationFailed");
                    }

                    //Set shoppingcart details like shipping. payment setting, etc.
                    SetShoppingCartDetails(submitOrderViewModel, userAddresses, cartModel);

                    bool isCreditCardPayment = false;
                    // Condition for "Credit Card" payment.
                    if (IsNotNull(cartModel?.Payment) && Equals(cartModel.Payment.PaymentName.ToLower(), ZnodeConstant.CreditCard.ToLower()))
                    {
                        isCreditCardPayment = true;
                        OrdersViewModel orderViewModel = ProcessCreditCardPayment(submitOrderViewModel, cartModel);
                        if (orderViewModel.HasError)
                        {
                            return orderViewModel;
                        }
                    }
                    // Condition for "PayPal Express".                   
                    else if (!string.IsNullOrEmpty(cartModel?.Payment?.PaymentName) && Equals(cartModel.Payment.PaymentName.Replace("_", "").ToLower(), ZnodeConstant.PayPalExpress.ToLower()))
                    {
                        ZnodeLogging.LogMessage($"Paypal Token - {submitOrderViewModel.PayPalToken}");
                        OrdersViewModel order = new OrdersViewModel();
                        if (string.IsNullOrEmpty(submitOrderViewModel.PayPalToken))
                        {
                            return PayPalExpressPaymentProcess(submitOrderViewModel, cartModel, userAddresses);
                        }
                        else
                        {
                            order = PayPalExpressPaymentProcess(submitOrderViewModel, cartModel, userAddresses);
                        }

                        if (!string.IsNullOrEmpty(order?.PayPalExpressResponseToken))
                        {
                            cartModel.Token = order.PayPalExpressResponseToken;
                        }
                        else
                        {
                            return order;
                        }
                    }
                    //Amazon payment.
                    else if (IsNotNull(submitOrderViewModel?.PaymentType) && Equals(submitOrderViewModel?.PaymentType.ToLower(), ZnodeConstant.AmazonPay.ToLower()) && !string.IsNullOrEmpty(submitOrderViewModel.AmazonPayReturnUrl) && !string.IsNullOrEmpty(submitOrderViewModel.AmazonPayCancelUrl))
                    {
                        return AmazonPaymentProcess(submitOrderViewModel, cartModel, userAddresses);
                    }

                    if (submitOrderViewModel.IsFromAmazonPay)
                    {
                        cartModel.Token = cartModel.Token;
                    }

                    if (!string.IsNullOrEmpty(submitOrderViewModel.PayPalToken) && submitOrderViewModel.IsFromPayPalExpress)
                    {
                        submitOrderViewModel.CardType = "PayPal";
                        submitOrderViewModel.TransactionId = cartModel.Token;
                    }

                    if (submitOrderViewModel.IsFromAmazonPay)
                    {
                        cartModel.Token = submitOrderViewModel.PaymentToken;
                        submitOrderViewModel.CardType = "Amazon";
                        submitOrderViewModel.TransactionId = submitOrderViewModel.PaymentToken;
                    }

                    //Card Type
                    cartModel.CardType = submitOrderViewModel.CardType;
                    cartModel.CcCardExpiration = submitOrderViewModel.CcExpiration;
                    cartModel.TransactionId = submitOrderViewModel.TransactionId;
                    if (IsNotNull(PortalAgent.CurrentPortal.PublishState))
                        cartModel.PublishStateId = (byte)PortalAgent.CurrentPortal.PublishState;

                    cartModel.IsOrderFromWebstore = true;

                    OrdersViewModel _ordersViewModel = PlaceOrder(cartModel);

                    //Update the new balance values against the user.
                    if (_ordersViewModel.OmsOrderId > 0)
                    {
                        RemoveCookie(WebStoreConstants.UserOrderReceiptOrderId);
                        SaveInCookie(WebStoreConstants.UserOrderReceiptOrderId, Convert.ToString(_ordersViewModel.OmsOrderId), ZnodeConstant.MinutesInAHour);
                        UpdateUserDetailsInSession(_ordersViewModel.Total);
                    }

                    //Get address from cache.
                    string cacheKey = $"{WebStoreConstants.UserAccountAddressList}{cartModel.UserId}";
                    Helper.ClearCache(cacheKey);

                    if (isCreditCardPayment && !cartModel.IsGatewayPreAuthorize && _ordersViewModel.OmsOrderId > 0 && !string.IsNullOrEmpty(cartModel.Token))
                    {
                        CapturePayment(_ordersViewModel.OmsOrderId, cartModel.Token, _ordersViewModel);
                    }
                    else if (isCreditCardPayment)
                        _orderClient.CreateOrderHistory(new OrderHistoryModel() { OmsOrderDetailsId = _ordersViewModel.OmsOrderDetailsId, Message = "TextTransactionPreAuthorized", TransactionId = _ordersViewModel.TransactionId, CreatedBy = _ordersViewModel.CreatedBy, ModifiedBy = _ordersViewModel.ModifiedBy });

                    if (IsNotNull(submitOrderViewModel?.PaymentType) && Equals(submitOrderViewModel?.PaymentType.ToLower(), ZnodeConstant.AmazonPay.ToLower()) && !cartModel.IsGatewayPreAuthorize && _ordersViewModel.OmsOrderId > 0 && !string.IsNullOrEmpty(cartModel.Token))
                    {
                        CapturePayment(_ordersViewModel.OmsOrderId, submitOrderViewModel.PaymentToken, _ordersViewModel);
                    }

                    return _ordersViewModel;
                }
            }
            return new OrdersViewModel() { HasError = true, ErrorMessage = "ErrorFailedToCreate" };
        }
             
    }
}
