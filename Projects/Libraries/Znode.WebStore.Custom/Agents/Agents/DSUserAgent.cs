﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class DSUserAgent: UserAgent, IUserAgent
    {

        private readonly IOrderClient _orderClient;
        private readonly IUserClient _userClient;
        private readonly IShippingClient _shippingClient;
        public DSUserAgent(ICountryClient countryClient, IWebStoreUserClient webStoreAccountClient, IWishListClient wishListClient, IUserClient userClient, IPublishProductClient productClient, ICustomerReviewClient customerReviewClient, IOrderClient orderClient,
          IGiftCardClient giftCardClient, IAccountClient accountClient, IAccountQuoteClient accountQuoteClient, IOrderStateClient orderStateClient, IPortalCountryClient portalCountryClient, IShippingClient shippingClient, IPaymentClient paymentClient, ICustomerClient customerClient, IStateClient stateClient, IPortalProfileClient portalProfileClient) :
            base(countryClient, webStoreAccountClient, wishListClient, userClient, productClient, customerReviewClient, orderClient, giftCardClient, accountClient, accountQuoteClient,
                orderStateClient, portalCountryClient, shippingClient, paymentClient, customerClient, stateClient, portalProfileClient)
        {
            _orderClient = GetClient<IOrderClient>(orderClient);
            _userClient = GetClient<IUserClient>(userClient);
            _shippingClient = GetClient<IShippingClient>(shippingClient);
        }
        public override OrdersViewModel GetOrderDetails(int orderId, int portalId = 0)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, PortalAgent.CurrentPortal.PortalId.ToString());

            UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

            if (HelperUtility.IsNotNull(userViewModel) && orderId > 0)
            {
                OrderModel orderModel = portalId > 0 ? GetOrderBasedOnPortalId(orderId, portalId) : _orderClient.GetOrderById(orderId, SetExpandsForReceipt(), filters);

                if (orderModel.OmsOrderId > 0 || userViewModel.UserId == orderModel.UserId || ((orderModel.IsQuoteOrder || userViewModel?.AccountId.GetValueOrDefault() > 0)))
                {
                    List<OrderLineItemModel> orderLineItemListModel = new List<OrderLineItemModel>();

                    //Create new order line item model.
                    CreateSingleOrderLineItem(orderModel, orderLineItemListModel);

                    orderModel.OrderLineItems = orderLineItemListModel;
                    /*Here*/
                    orderModel.TrackingNumber = SetTrackingURL(orderModel);
                    if (orderModel?.OrderLineItems?.Count > 0)
                    {
                        OrdersViewModel orderDetails = orderModel?.ToViewModel<OrdersViewModel>();
                        orderDetails.CurrencyCode = PortalAgent.CurrentPortal?.CurrencyCode;
                        orderDetails.CouponCode = orderDetails.CouponCode?.Replace("<br/>", ", ");
                        orderDetails?.OrderLineItems?.ForEach(x => x.UOM = orderModel?.ShoppingCartModel?.ShoppingCartItems?.Where(y => y.SKU == x.Sku).Select(y => y.UOM).FirstOrDefault());
                        /*Here*/
                        orderDetails?.OrderLineItems?.ForEach(x => x.TrackingNumber = SetTrackingUrl(x.TrackingNumber, orderModel.ShippingId));
                        return orderDetails;
                    }
                }
                return null;
            }
            return new OrdersViewModel();
        }
        private string SetTrackingUrl(string trackingNo, int shippingId)
        {
            string trackingUrl = GetTrackingUrlByShippingId(shippingId);
            return string.IsNullOrEmpty(trackingUrl) ? trackingNo : "<a target=_blank href=" + GetTrackingUrlByShippingId(shippingId) + trackingNo + ">" + trackingNo + "</a>";
        }
        private string GetTrackingUrlByShippingId(int shippingId)
            => _shippingClient.GetShipping(shippingId)?.TrackingUrl;
        /*nivi code start*/
        private string SetTrackingURL(OrderModel ordermodel)
        {
            string TrackingNumbers = "";
            try
            {
                //Get shipping type name based on provided shipping id
                string SPEEDY = Convert.ToString(ConfigurationManager.AppSettings["SPEEDYTrackingUrl"]);

                string[] shipping = ordermodel.Custom1?.Split(',');
                string[] TrackNumbers = ordermodel.TrackingNumber?.Split(',');
                //string shippingType = GetShippingType(ordermodel.ShippingId);
                int icount = 0;
                if (TrackNumbers?.Count() > 0)
                {
                    foreach (string shippingname in shipping)
                    {
                        if (ZnodeConstant.UPS == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers + $"<a target=_blank href={ ZnodeApiSettings.UPSTrackingURL }>{TrackNumbers[icount]} </a >" + ",";
                        else if (ZnodeConstant.FedEx == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers + $"<a target=_blank href={ ZnodeApiSettings.FedExTrackingURL }>{TrackNumbers[icount]} </a >" + ",";
                        else if (ZnodeConstant.USPS == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers + $"<a target=_blank href={ ZnodeApiSettings.USPSTrackingURL }>{TrackNumbers[icount]} </a >" + ",";
                        else if ("SPEEDEE" == shippingname)
                            TrackingNumbers = TrackingNumbers + $"<a target=_blank href={ SPEEDY + TrackNumbers[icount]}>{TrackNumbers[icount]} </a >" + ",";
                        icount = icount + 1;
                    }
                    if (TrackingNumbers != "")
                    {
                        TrackingNumbers = TrackingNumbers.Remove(TrackingNumbers.Length - 1, 1);
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Shipping=" + Convert.ToString(ordermodel.Custom1) + " TrackingNo=" + ordermodel.TrackingNumber + "error =" + ex.Message + " stacktrace =" + ex.StackTrace, string.Empty, TraceLevel.Error);
            }
            return TrackingNumbers;
        }
        //The parameters isShippingBillingDifferent and isCreateAccountForGuestUser are not in use currently but for compatibility we have added these paramter, In future release we will remove these parameter
        // Create/Update the address of the user.       
        public override AddressViewModel CreateUpdateAddress(AddressViewModel addressViewModel, string addressType = null, bool isShippingBillingDifferent = false, bool isCreateAccountForGuestUser = false)
        {
            try
            {
                SetBillingShippingFlags(addressViewModel, addressType);
                UserViewModel model = GetUserViewModelFromSession();

                #region Checks to validate address.               

                BooleanModel booleanModel = IsValidAddress(addressViewModel);
                //Validate address if enable address validation flag is set to true.

                //Nivi status change for address validation//
                booleanModel.IsSuccess = true;

                if (!booleanModel.IsSuccess)
                    return (AddressViewModel)GetViewModelWithErrorMessage(addressViewModel, booleanModel.ErrorMessage ?? "AddressValidationError");

                if (isShippingBillingDifferent && isCreateAccountForGuestUser) addressViewModel.IsShippingBillingDifferent = true;

                if (HelperUtility.IsNotNull(model) && !isShippingBillingDifferent)
                    //Validate default address.
                    addressViewModel = ValidateDefaultAddress(addressViewModel, model);

                //Nivi HasError change for address validation//
                addressViewModel.HasError = false;

                if (addressViewModel.HasError)
                    return addressViewModel;

                //if user is null then save address for anonymous user or
                // if aspnetUserId is null then save address for anonymous user 
                if (model?.AspNetUserId == null || (model?.UserId).GetValueOrDefault() < 1)
                    return SetAnonymousUserAddresses(addressViewModel, addressType);
                #endregion

                addressViewModel.UserId = model.UserId;
                addressViewModel.AccountId = model.AccountId.GetValueOrDefault();

                //Create/Update address.
                addressViewModel = CreateUpdateAddress(addressViewModel);

                //Remove address from cache and session.
                if (addressViewModel?.AddressId > 0)
                    RemoveAddressFromCacheAndSession(addressViewModel);
                return addressViewModel;
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.InvalidData:
                        return (AddressViewModel)GetViewModelWithErrorMessage(new AddressViewModel(), "ErrorAddUpdateAddressNewCustomer");
                    default:
                        return (AddressViewModel)GetViewModelWithErrorMessage(new AddressViewModel(), "ErrorFailedToCreate");
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return (AddressViewModel)GetViewModelWithErrorMessage(new AddressViewModel(),"ErrorFailedToCreate");
            }
        }

       
       
    }
}
