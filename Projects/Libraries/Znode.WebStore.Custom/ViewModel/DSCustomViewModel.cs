﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore;

namespace Znode.WebStore.Custom.ViewModel
{
    public class DSCustomViewModel : BaseViewModel
    {
        public string Size { get; set; }
        public string Inventory { get; set; }
        public string Drewwidth { get; set; }
        public int ParentProductId { get; set; }
        public string SKU { get; set; }
        public decimal? MaxQuantity { get; set; }
        public decimal? MinQuantity { get; set; }
        public DateTime BackOrderExpectedDate { get; set; }
    }
}
