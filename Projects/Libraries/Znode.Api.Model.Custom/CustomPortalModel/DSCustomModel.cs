﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model
{
    public class DSCustomModel : BaseModel
    {
        public string Size { get; set; }
        public string Inventory { get; set; }
        public string Drewwidth { get; set; }
        public int ParentProductId { get; set; }
        public string SKU { get; set; }
        public decimal? MaxQuantity { get; set; }
        public decimal? MinQuantity { get; set; }
        public DateTime BackOrderExpectedDate { get; set; }
    }
}
