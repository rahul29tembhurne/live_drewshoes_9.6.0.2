﻿using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model
{
    public class CustomPortalModel: BaseModel
    {
        public PortalModel Portal { get; set; }
        public CustomPortalDetailModel PortalCustomDetail { get; set; }
    }
}
