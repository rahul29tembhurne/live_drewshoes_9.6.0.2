﻿//using DevExpress.XtraPrinting.Native;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.ERPConnector.EverestConnector;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.ShoppingCart;
using System.Web.Mvc;
//using System.Net.Mail;

namespace Znode.Engine.ERPConnector
{
    public class ZnodeEverestConnector : BaseERP
    {
        private readonly IZnodeRepository<ZnodeImportHead> _importHeadRepository;
        private readonly IZnodeRepository<ZnodeERPConfigurator> _eRPConfiguratorRepository;
        private readonly IZnodeRepository<ZnodeOmsOrder> _omsOrderRepository;
        private IOrderClient _orderClient;
        private readonly IZnodeRepository<ZnodeImportTemplate> _importTemplateRepository;
        private readonly IZnodeRepository<ZnodeImportProcessLog> _importProcessLogRepository;
        
        public ZnodeEverestConnector()
        {
            _importHeadRepository = new ZnodeRepository<ZnodeImportHead>();
            _eRPConfiguratorRepository = new ZnodeRepository<ZnodeERPConfigurator>();
            _orderClient = GetClient<OrderClient>();
            _omsOrderRepository = new ZnodeRepository<ZnodeOmsOrder>();
            _importTemplateRepository = new ZnodeRepository<ZnodeImportTemplate>();
            _importProcessLogRepository = new ZnodeRepository<ZnodeImportProcessLog>();
        }

        [RealTime("RealTime")]
        public string UpdateOrder()
        {
            return "";
        }
               
        public override bool ARHistory() => true;

        //AR Payment Details from ERP to ZNODE
        public override bool ARPaymentDetails() => true;

        //AR Balance from ERP to ZNODE
        public override bool ARBalance() => true;

        //Refresh Product Content from ERP to Znode
        public override bool ProductRefresh() => true;

        //Refresh Category from ERP to Znode
        public override bool CategoryRefresh() => true;

        //Refresh Product Category Link from ERP to Znode                            
        public override bool ProductCategoryLinkRefresh() => true;

        //Refresh  Contact List from ERP to Znode          
        public override bool ContactListRefresh() => true;

        //Get Contact List on real-time from ERP to Znode   
        public override bool GetContactList() => true;

        //Refresh Contact Details from ERP to Znode           
        public override bool ContactDetailsRefresh() => true;

        //Get Contact Details on real-time from   ERP to Znode  
        public override bool GetContactDetails() => true;

        //Get Login from ZNODE to ERP
        public override bool Login() => true;

        //Create Contact from ZNODE to ERP
        public override bool CreateContact() => true;

        //Create Update from ZNODE to ERP
        public override bool UpdateContact() => true;

        public override bool PaymentAuthorization() => true;
        public override bool SaveCreditCard() => true;

        //Refresh customer details from ERP to Znode
        public override bool CustomerDetailsRefresh() => true;

        //Get Customer Details on real-time from   ERP to Znode  
        public override bool GetCustomerDetails() => true;

        //Refresh ShipToList from ERP to Znode
        public override bool ShipToListRefresh() => true;

        //Get ShipToList on real-time from   ERP to Znode  
        public override bool GetShipToList() => true;

        //Locate My Account Or Match Customer from Znode to ERP
        public override bool LocateMyAccountOrMatchCustomer() => true;

        //Create Customer from Znode to ERP
        public override bool CreateCustomer() => true;

        //Update Customer from Znode to ERP
        public override bool UpdateCustomer() => true;

        //Create SHIPTO from Znode to ERP
        public override bool CreateSHIPTO() => true;

        //Update SHIPTO from Znode to ERP
        public override bool UpdateSHIPTO() => true;

        //private string GetXMLFilePath(string fileName)
        // => $"{GetDestinationUrl("EverestFolderPath")}/{fileName}.xml";


        public virtual bool UpdateShipmentDetails()
        {
            ZnodeLogging.LogMessage("UPDATESHIPMENTDETAILS STARTED", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
            bool status = false;
            try
            {
                XmlDocument xmldoc = new XmlDocument();

                string xmlPath = (GetXMLFilePath("UPDTRACK", "Shipment"));
                FileStream fs = new FileStream(xmlPath, FileMode.Open, FileAccess.Read);
                xmldoc.Load(fs);

                ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
                objStoredProc.GetParameter("@xml", new SqlXml(new XmlTextReader(xmldoc.InnerXml, XmlNodeType.Document, null)), ParameterDirection.Input, SqlDbType.Xml);
                DataSet data = objStoredProc.GetSPResultInDataSet("Drew_UpdateOrderInfo");
                DataTable processedOrderDetails = data.Tables[0];
                ZnodeLogging.LogMessage("UPDATESHIPMENTDETAILS SP EXECUTED ROWCOUNT RECEIVED=" + processedOrderDetails.Rows.Count, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                status = true;
                foreach (DataRow xn in processedOrderDetails.Rows)
                {
                    string receipthtml = "";
                    int OrderId = Convert.ToInt32(xn["OrderId"]);
                    OrderModel orderModel = _orderClient.GetOrderById(OrderId, GetOrderExpands(), null);
                    List<OrderLineItemModel> OrderLineItemstoberemoved = orderModel.OrderLineItems.Where(x => x.Quantity == 0).ToList();
                    OrderLineItemstoberemoved.ForEach(x => orderModel.OrderLineItems.Remove(x));

                    //Method to get Email Template.
                    EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("ShippingReceipt", orderModel.PortalId);
                    if (emailTemplateMapperModel != null)
                    {
                        ZnodeOrderReceipt receipt = new ZnodeOrderReceipt();
                        receipthtml = EmailTemplateHelper.ReplaceTemplateTokens(GenerateHtmlResendReceiptWithParser(emailTemplateMapperModel.Descriptions, orderModel));
                        SendOrderStatusEmail(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, receipthtml);
                    }
                }
                ZnodeLogging.LogMessage("UPDATESHIPMENTDETAILS MAILS SENT", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                /*New*/
                fs.Close();
                MoveFileToArchiveOnSuccess("UPDTRACK", "Shipment");
                ZnodeLogging.LogMessage("UPDATESHIPMENTDETAILS FILE MOVED TO ARCHIVES", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("UPDATESHIPMENTDETAILS ERROR!!-"+ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
            }
            return status;
        }

        //public virtual bool UpdateShipmentDetails()
        //{
        //        bool status = false;
        //    try
        //    {


        //        XmlDocument xmldoc = new XmlDocument();

        //        string xmlPath = (GetXMLFilePath("UPDTRACK","Shipment"));
        //        FileStream fs = new FileStream(xmlPath, FileMode.Open, FileAccess.Read);
        //        xmldoc.Load(fs);

        //    //SqlConnection conn = GetSqlConnection();
        //    //SqlCommand cmd = new SqlCommand("Drew_UpdateOrderInfo", conn);
        //    //cmd.CommandType = CommandType.StoredProcedure;
        //    //cmd.Parameters.AddWithValue("@xml", new SqlXml(new XmlTextReader(xmldoc.InnerXml, XmlNodeType.Document, null)));
        //    //if (conn.State.Equals(ConnectionState.Closed))
        //    //{
        //    //    conn.Open();
        //    //}

        //    //cmd.ExecuteReader();
        //    ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
        //    objStoredProc.GetParameter("@xml", new SqlXml(new XmlTextReader(xmldoc.InnerXml, XmlNodeType.Document, null)), ParameterDirection.Input, SqlDbType.Xml);
        //    DataSet data = objStoredProc.GetSPResultInDataSet("Drew_UpdateOrderInfo");
        //    //DataTable processedRecordscount = data.Tables[0];
        //    DataTable processedOrderDetails = data.Tables[0];

        //    status = true;
        //        //XmlNodeList xnList = xmldoc.SelectNodes("/Order/Header");
        //        // string receipthtml = _emailTemplateRepository.Table.FirstOrDefault(x => x.Subject == "Shipping Receipt").Content;
        //        foreach (DataRow xn in processedOrderDetails.Rows)
        //        {
        //         string receipthtml = "";
        //        //int portalid = Convert.ToInt32(xn["PortalId"]);
        //        //string useremailid = Convert.ToString(xn["BillingEmailId"]);
        //        //string orderNumber = xn["WebOrderNo"]
        //        int OrderId = Convert.ToInt32(xn["OrderId"]);
        //        OrderModel orderModel = _orderClient.GetOrderById(OrderId, GetOrderExpands(), null);
        //        List<OrderLineItemModel> OrderLineItemstoberemoved = orderModel.OrderLineItems.Where(x => x.Quantity == 0).ToList();
        //        OrderLineItemstoberemoved.ForEach(x => orderModel.OrderLineItems.Remove(x));

        //        //Method to get Email Template.
        //        EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("ShippingReceipt", orderModel.PortalId);
        //            if (emailTemplateMapperModel != null)
        //            {                    
        //                ZnodeOrderReceipt receipt = new ZnodeOrderReceipt();
        //                receipthtml = EmailTemplateHelper.ReplaceTemplateTokens(GenerateHtmlResendReceiptWithParser(emailTemplateMapperModel.Descriptions, orderModel));                       
        //                SendOrderStatusEmail(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, receipthtml);
        //            }     
        //        }               
        //        /*New*/
        //        fs.Close();
        //        MoveFileToArchiveOnSuccess("UPDTRACK", "Shipment");
        //    }
        //    catch (Exception ex)
        //    {
        //        ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
        //    }
        //    return status;
        //}

        //This method will provide the SQL connection
        private SqlConnection GetSqlConnection() => new SqlConnection(HelperMethods.ConnectionString);
        //Refresh inventory for products by warehouse/DC from ERP to Znode on a scheduled basis.
        //Inventory Refresh - Batch Process.
        public override bool InventoryRefresh()
        {
            bool status = false;
            Guid tableGuid = Guid.NewGuid();
            int? importHeadId = _importHeadRepository.Table.Where(y => y.Name == "Inventory")?.FirstOrDefault().ImportHeadId;

            //Import MAGINV.
            ImportFileData(EverestCSVNames.InventoryBySKU, tableGuid, importHeadId,"Inventory");


            ZnodeLogging.LogMessage("Process for inventory refresh started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            SqlConnection conn = GetSqlConnection();
            try
            {
                SqlCommand cmd = new SqlCommand("Znode_ImportProcessData", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ImportHeadId", importHeadId);
                cmd.Parameters.AddWithValue("@TblGUID", tableGuid);
                cmd.Parameters.AddWithValue("@TouchPointName", GetCurrentMethod());
                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                }

                cmd.ExecuteReader();

                status = true;
                //GenerateLogs("Inventory Refresh Status",importHeadId);
                ZnodeLogging.LogMessage("Process for inventory refresh completed successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                /*New*/
                MoveFileToArchiveOnSuccess(EverestCSVNames.InventoryBySKU, "Inventory");
                return status;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                return false;
            }
        }

        #region Private Methods
        //private string GetCSVFilePath(string fileName)
        //=> $"{GetDestinationUrl("EverestFolderPath")}/{fileName}.csv";
        /*NEW*/
        private void GenerateLogs(string subject,int? importHeadId)
        {
            int importTemplateId = _importTemplateRepository.Table.FirstOrDefault(x => x.ImportHeadId == importHeadId).ImportTemplateId;
            ZnodeImportProcessLog logdetails = _importProcessLogRepository.Table.OrderByDescending(x => x.ImportProcessLogId).FirstOrDefault(x => x.ImportTemplateId == importTemplateId);
            if (logdetails != null)
            {
                StringBuilder strHtml = new StringBuilder();
                strHtml.Append("<div class='col-sm-12 list-container'>");
                strHtml.Append("<table style='border:1px solid black;'> <thead><tr>");
                strHtml.Append("<th colspan=3 style='text-align:center;'>"+ subject + "-" + logdetails.Status + "</th>");
                strHtml.Append("</tr></thead>");
                strHtml.Append("<tr>");                
                strHtml.Append("</tr>");
                strHtml.Append("<tr style='border:1px solid black;'>");
                strHtml.Append("<td style='border:1px solid black;'><b>  Total Records Processed  </b></td>");
                strHtml.Append("<td style='border:1px solid black;'><b>  Total Records Succeeded  </b></td>");
                strHtml.Append("<td style='border:1px solid black;'><b>  Total Records Failed     </b></td>");
                strHtml.Append("</tr>");
                strHtml.Append("<tr>");
                strHtml.Append("<td style='border:1px solid black;text-align:center;'><b>" + logdetails.TotalProcessedRecords + "</b></td>");
                strHtml.Append("<td style='border:1px solid black;text-align:center;'><b>" + logdetails.SuccessRecordCount + "</b></td>");
                strHtml.Append("<td style='border:1px solid black;text-align:center;text-align:center;'><b>" + logdetails.FailedRecordcount + "</b></td>");
                strHtml.Append("</tr>");
                strHtml.Append("</table> ");
                strHtml.Append("</div>");
                ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                MvcHtmlString s = MvcHtmlString.Create(strHtml.ToString());
                try
                {
                    ZnodeEmail.SendEmail(ZnodeConfigManager.SiteConfig.AdminEmail,"bageshree.kherdekar@nivisolutions.com",string.Empty, subject, s.ToString(), true);
                }
                catch(Exception ex)
                {

                }
            }
        }
        public bool SendMail(int portalId, string userEmailId, string subject, string senderEmail, string bccEmailId, string receiptHtml, bool isEnableBcc = false)
        {
            bool isSuccess = false;
            try
            {
                //ZnodeLogging.LogMessage("SendOrderReceipt called for userEmailId." + userEmailId, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                //ZnodeLogging.LogMessage("SendOrderReceipt senderEmail." + senderEmail, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                //This method is used to send an email.
                ZnodeEmail.SendEmail(userEmailId, senderEmail, string.Empty, subject, receiptHtml, true);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Can not send the Order Receipt. Please verify the SMTP setting.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error, ex);
            }
            return isSuccess;
        }
        private bool MoveFileToArchiveOnSuccess(string filename, string foldername)
        {
            try
            {
                string fileToMove = "";
                if (filename == "UPDTRACK")
                    fileToMove = GetXMLFilePath(filename, foldername);
                else
                    fileToMove = GetCSVFilePath(filename, foldername);
                string destinationPath = GetArchiveFolderPath(filename, foldername);

                string today = String.Join("_", DateTime.Now.ToString().Split('/'));
                string todaydatetime = String.Join("_", today.Split(':'));
                string moveTo = destinationPath + @"/" + todaydatetime + "_" + filename+".csv";
                //moving file
                File.Move(fileToMove, moveTo);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private string GetCSVFilePath(string fileName, string foldername)
        {
            return $"{GetDestinationUrl("EverestFolderPath")}/{foldername}/{fileName}.csv";

        }
        private string GetArchiveFolderPath(string fileName, string foldername)
        {
            return $"{GetDestinationUrl("EverestFolderPath")}\\{foldername}\\Archives";
        }
        private string GetXMLFilePath(string fileName, string folderPath)
        {
          return  $"{GetDestinationUrl("EverestFolderPath")}/{folderPath}/{fileName}.xml";
        }
        //Get the port values.
        private string GetDestinationUrl(string portName)
        {
            string Json = _eRPConfiguratorRepository.Table.FirstOrDefault(x => x.IsActive == true)?.JsonSetting;
            NameValueCollection nameValueColl = new NameValueCollection();
            JObject obj = JObject.Parse(Json);
            string erpLinkUrlValue = string.Empty;
            foreach (JToken item in obj[ZnodeConstant.ERPConnectorControlList])
            {
                nameValueColl.Add(item[ZnodeConstant.Name].ToString(), item[ZnodeConstant.Value].ToString());
            }
            // nameValueColl.Add(item[ZnodeConstant.Name].ToString(), DecryptData(item[ZnodeConstant.Value].ToString()));
            if (portName.Equals("EverestFolderPath"))
            {
                return $"{ nameValueColl["EverestFolderPath"]}";
            }          

            return "";
        }
        private void ReadRowsWithoutHeader(DataTable dataTable, string[] rows, string headers, string fileLocation)
        {
            string[] headerArray = headers?.Split(',');

            for (int j = 0; j < headerArray?.Count(); j++)
            {
                dataTable.Columns.Add(headerArray[j]); //add headers  
            }

            for (int i = 0; i < rows.Count(); i++)
            {
              
                string[] rowValues = (fileLocation == "TPRICE") || (fileLocation == "MAGINV") ? rows[i].Split(',') : (fileLocation == "MAGSOLD") ||
                (fileLocation == "MAGSHIP") ? rows[i].Split('|') : rows[i].Split(','); //split each row with comma or tilde to get individual values  
                {
                    #region Actual Code
                    try
                    {
                        DataRow dr = dataTable.NewRow();
                        for (int k = 0; k < rowValues.Count(); k++)
                        {
                            dr[k] = Convert.ToString(rowValues[k].Replace('"', ' ').Trim());
                        }

                        dataTable.Rows.Add(dr); //add other rows 
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage("i" + i.ToString(), ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    }
                    #endregion
                }
            }
        }
        //Import ABS related files.
        private void ImportFileData(string fileLocation, Guid tableGuid, int? importHeadId,string foldername)
        {
            try
            {

                ZnodeLogging.LogMessage("Import Data--" + fileLocation, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
                ZnodeLogging.LogMessage("Import Data--" + importHeadId, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);

                //Read the csv file path.// (GetCSVFilePath(fileLocation));
                string csvPath = (GetCSVFilePath(fileLocation, foldername));// @"E:\WorkSpace\AWCT changes\ABS_07232019\" + fileLocation + ".CSV";

                DataTable dataTable = new DataTable();
                string Fulltext;


                using (StreamReader sr = new StreamReader(csvPath))
                {
                    IZnodeViewRepository<string> objStoredProc = new ZnodeViewRepository<string>();
                    //Get the headers of current csv.
                    IList<string> headers = objStoredProc.ExecuteStoredProcedureList($"Znode_ImportGetTableDetails {fileLocation},{importHeadId}");
                    while (!sr.EndOfStream)
                    {
                        Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                        string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                      
                        ReadRowsWithoutHeader(dataTable, rows, headers.FirstOrDefault(), fileLocation);

                    }

                    string uniqueIdentifier = string.Empty;
                    //Create hash table for the respective csv's.
                    ReadAndCreateTable(out uniqueIdentifier, headers.FirstOrDefault(), dataTable, tableGuid, fileLocation);

                }

            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Import Data err--" + ex.Message + "  strackTrace:=" + ex.StackTrace, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
            }
        }

        //Create hash tables in database for ABS files import.
        private string ReadAndCreateTable(out string uniqueIdentifier, string headers, DataTable dt, Guid tableGuid, string csvName)
        {
            string tableName = string.Empty;
            try
            {
                uniqueIdentifier = string.Empty;
                string[] columnNames = headers?.Split(',')?.ToArray();
                //generate the doble hash temp table name
                if (columnNames?.Count() > 0)
                {

                    tableName = $"tempdb..[##{csvName}_{tableGuid}]";

                    //add the same guid in datatable
                    DataColumn guidCol = new DataColumn("guid", typeof(String));
                    guidCol.DefaultValue = tableGuid;
                    dt.Columns.Add(guidCol);

                    //create the doble hash temp table
                    MakeTable(tableName, GetTableColumnsFromFirstLine(columnNames));

                    //assign the guid to out parameter
                    uniqueIdentifier = Convert.ToString(tableGuid);
                }

                //If table created and it has headers then dump the CSV data in doble hash temp table
                if (!string.IsNullOrEmpty(tableName) && columnNames.Count() > 0)
                {
                    SaveDataInChunk(dt, tableName);
                }
                return tableName;
            }
            catch (Exception ex)
            {
                uniqueIdentifier = string.Empty;
                ZnodeLogging.LogMessage("Product Refesh ReadAndCreateTable Data err--" + ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Warning);
                return uniqueIdentifier;
            }

        }
        //This method will create ##temp table in SQL
        private void MakeTable(string tableName, string columnList)
        {
            SqlConnection conn = GetSqlConnection();
            SqlCommand cmd = new SqlCommand("CREATE TABLE " + tableName + "  " + columnList + " ", conn);

            if (conn.State.Equals(ConnectionState.Closed))
            {
                conn.Open();
            }

            cmd.ExecuteNonQuery();
        }

        //This method will create the table columns and append the datatype to the column headers
        private string GetTableColumnsFromFirstLine(string[] firstLine)
        {
            StringBuilder sbColumnList = new StringBuilder();
            sbColumnList.Append("(");
            sbColumnList.Append(string.Join(" nvarchar(max) , ", firstLine));
            sbColumnList.Append(" nvarchar(max), guid nvarchar(max) )");
            return sbColumnList.ToString();
        }

        //This method will divide the data in chunk.  The chunk size is 5000.
        private void SaveDataInChunk(DataTable dt, string tableName)
        {
            if (HelperUtility.IsNotNull(dt) && dt.Rows?.Count > 0)
            {
                int chunkSize = int.Parse(ConfigurationManager.AppSettings["ZnodeImportChunkLimit"].ToString());
                int startIndex = 0;
                int totalRows = dt.Rows.Count;
                int totalRowsCount = totalRows / chunkSize;

                if (totalRows % chunkSize > 0)
                {
                    totalRowsCount++;
                }

                for (int iCount = 0; iCount < totalRowsCount; iCount++)
                {
                    DataTable fileData = dt.Rows.Cast<DataRow>().Skip(startIndex).Take(chunkSize).CopyToDataTable();
                    startIndex = startIndex + chunkSize;
                    InsertData(tableName, fileData);
                }
            }
        }

        //This method will save the chunk data in ##temp table using Bulk upload
        private void InsertData(string tableName, DataTable fileData)
        {
            SqlConnection conn = GetSqlConnection();
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
            {
                bulkCopy.DestinationTableName = tableName;
                bulkCopy.BulkCopyTimeout = 20000;
                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                }

                bulkCopy.WriteToServer(fileData);
                conn.Close();
            }
        }
        #endregion

        #region For Mail
        public virtual string GenerateHtmlResendReceiptWithParser(string receiptHtml, OrderModel orderModel)
        {
            try
            {
                ZnodeOrderReceipt receipt = new ZnodeOrderReceipt();
                if (string.IsNullOrEmpty(receiptHtml))
                    return receiptHtml;

                //order to bind order details in data tabel
                DataTable orderTable = receipt.SetOrderData(orderModel);

                //create order line Item
                DataTable orderlineItemTable = receipt.CreateOrderLineItemTable();

                // create returned order line Item
                DataTable returnedOrderlineItemTable = receipt.CreateReturnedOrderLineItemTable();

                //order to bind order amount details in data tabel
                DataTable orderAmountTable = receipt.SetOrderAmountData(orderModel);

                // order to bind returned order amount details in data tabel
                DataTable returnedOrderAmountTable = receipt.SetReturnedOrderAmountData(orderModel);

                //create multiple Address
                DataTable multipleAddressTable = receipt.CreateOrderAddressTable();

                //create multiple tax address
                DataTable multipleTaxAddressTable = receipt.CreateOrderTaxAddressTable();

                //bind line item data
                receipt.BuildOrderLineItem(multipleAddressTable, orderlineItemTable, multipleTaxAddressTable, orderModel, returnedOrderlineItemTable);

                ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(receiptHtml);

                // Parse order table
                receiptHelper.Parse(orderTable.CreateDataReader());

                // Parse order line items table
                receiptHelper.Parse("AddressItems", multipleAddressTable.CreateDataReader());
                foreach (DataRow address in multipleAddressTable.Rows)
                {
                    // Parse OrderLineItem
                    var filterData = orderlineItemTable.DefaultView;
                    filterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
                    List<DataTable> group = filterData.ToTable().AsEnumerable()
                      .GroupBy(r => new { Col1 = r["GroupId"] })
                      .Select(g => g.CopyToDataTable()).ToList();

                    receiptHelper.ParseWithGroup("LineItems" + address["OmsOrderShipmentID"], group);

                    //Parse Tax based on order shipment
                    var amountFilterData = multipleTaxAddressTable.DefaultView;
                    amountFilterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
                    receiptHelper.Parse($"AmountLineItems{address["OmsOrderShipmentID"]}", amountFilterData.ToTable().CreateDataReader());
                }

                // Parse returned OrderLineItem
                var returnFilterData = returnedOrderlineItemTable.DefaultView;
                if (returnFilterData.Count > 0 && returnFilterData != null)
                    receiptHelper.Parse("ReturnLineItems", returnFilterData.ToTable().CreateDataReader());

                // Parse order amount table
                receiptHelper.Parse("GrandAmountLineItems", orderAmountTable.CreateDataReader());

                // Parse returned order amount table
                if (returnedOrderAmountTable.Rows.Count > 0 && returnedOrderAmountTable != null)
                    receiptHelper.Parse("ReturnedGrandAmountLineItems", returnedOrderAmountTable.CreateDataReader());
                //Replace the Email Template Keys, based on the passed email template parameters.

                return receiptHelper.Output;
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }

        protected EmailTemplateMapperModel GetEmailTemplateByCode(string code, int portalId = 0, int localeId = 0)
        {
            //Set Default Locale, in case no locale exists.
            if (localeId == 0)
                localeId = GetDefaultLocaleId();

            List<EmailTemplateMapperModel> emailTemplates = GetEmailTemplateList(code, portalId, localeId);

            if (!emailTemplates.Any())
            {
                localeId = GetDefaultLocaleId();

                List<EmailTemplateMapperModel> emailTemplatesByDefaultLocale = GetEmailTemplateList(code, portalId, localeId);
                return emailTemplatesByDefaultLocale.FirstOrDefault();
            }
            //Get the Configured Email Template Details based on the Template Code & Locale.
            return emailTemplates.FirstOrDefault();
        }

        private List<EmailTemplateMapperModel> GetEmailTemplateList(string code, int portalId, int localeId)
        {
            ZnodeRepository<ZnodeEmailTemplateArea> _emailTemplateAreaRepository = new ZnodeRepository<ZnodeEmailTemplateArea>();
            ZnodeRepository<ZnodeEmailTemplateMapper> _emailTemplateMapperRepository = new ZnodeRepository<ZnodeEmailTemplateMapper>();
            ZnodeRepository<ZnodeEmailTemplate> _emailTemplateRepository = new ZnodeRepository<ZnodeEmailTemplate>();
            ZnodeRepository<ZnodeEmailTemplateLocale> _emailTemplateLocaleRepository = new ZnodeRepository<ZnodeEmailTemplateLocale>();

            return (from emailTemplateArea in _emailTemplateAreaRepository.Table
                    join emailTemplateMapper in _emailTemplateMapperRepository.Table on emailTemplateArea.EmailTemplateAreasId equals emailTemplateMapper.EmailTemplateAreasId
                    join emailTemplate in _emailTemplateRepository.Table on emailTemplateMapper.EmailTemplateId equals emailTemplate.EmailTemplateId
                    join emailTemplateLocale in _emailTemplateLocaleRepository.Table on emailTemplate.EmailTemplateId equals emailTemplateLocale.EmailTemplateId
                    where emailTemplateArea.Code == code && emailTemplateMapper.PortalId == portalId && emailTemplateMapper.IsActive && emailTemplateLocale.LocaleId == localeId
                    select new EmailTemplateMapperModel()
                    {
                        Code = emailTemplateArea.Code,
                        EmailTemplateId = emailTemplate.EmailTemplateId,
                        EmailTemplateAreasId = emailTemplateArea.EmailTemplateAreasId,
                        TemplateName = emailTemplate.TemplateName,
                        Descriptions = emailTemplateLocale.Content,
                        Subject = emailTemplateLocale.Subject,
                        IsEnableBcc = emailTemplateMapper.IsEnableBcc,
                    }).ToList();
        }

        private int GetDefaultLocaleId()
        {
            ZnodeRepository<ZnodeLocale> _localeRepository = new ZnodeRepository<ZnodeLocale>();
            return _localeRepository.Table.Where(x => x.IsDefault == true).Select(x => x.LocaleId).FirstOrDefault();
        }

        public ExpandCollection GetOrderExpands()
        {
            ExpandCollection expands = new ExpandCollection();
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsPaymentState.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrder.ToString().ToLower());
            expands.Add(ExpandKeys.ZnodeShipping);
            expands.Add(ExpandKeys.ZnodeUser);
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsHistories.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString().ToLower());
            return expands;
        }

        public void SendOrderStatusEmail(int portalId, string emailAddress, string receipthtml)
        {
            //ZnodeLogging.LogMessage("SendOrderStatusEmail called for userEmailId="+ emailAddress, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            string subject = "Drew Shoe-Shipping Notification";
            SendOrderReceipt(portalId, emailAddress, subject, ZnodeConfigManager.SiteConfig.AdminEmail, string.Empty, receipthtml, false);
        }

        public bool SendOrderReceipt(int portalId, string userEmailId, string subject, string senderEmail, string bccEmailId, string receiptHtml, bool isEnableBcc = false)
        {
            bool isSuccess = false;
            try
            {
               // ZnodeLogging.LogMessage("SendOrderReceipt called for userEmailId." + userEmailId, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
               // ZnodeLogging.LogMessage("SendOrderReceipt senderEmail." + senderEmail, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                //This method is used to send an email.
                ZnodeEmail.SendEmail(userEmailId, senderEmail, string.Empty, subject, receiptHtml, true);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Can not send the Order Receipt. Please verify the SMTP setting.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error, ex);
            }
            return isSuccess;
        }
        #endregion

        //Get inventory on real-time from ERP to Znode              
        public override bool GetInventoryRealtime() => true;

        //Get Invoice History from ERP to Znode       
        public override bool InvoiceHistory() => true;

        //Get Invoice Details Status from ERP to Znode       
        public override bool InvoiceDetailsStatus() => true;

        public override bool RequestACatalog() => true;

        //Submit A Prospect from ERP to Znode       
        public override bool SubmitAProspect() => true;

        //OrderSimulate from Znode  to ERP      
        public override bool OrderSimulate() => true;

        //Create Order from Znode  to ERP   
        public override bool OrderCreate() => true;

        //Get Order History from ERP to Znode       
        public override bool OrderHistory() => true;

        //Get Order Details Status from ERP to Znode       
        public override bool OrderDetailsStatus() => true;

        //Pay Online from Znode to ERP
        public override bool PayOnline() => true;

        //Refresh standard price list from ERP to Znode on a scheduled basis.
        public override bool PricingStandardPriceListRefresh()
        {
            bool status = false;
            Guid tableGuid = Guid.NewGuid();
            int? importHeadId = _importHeadRepository.Table.Where(y => y.Name == "Pricing")?.FirstOrDefault().ImportHeadId;

            //Import TPRICE.
            ImportFileData(EverestCSVNames.Pricing, tableGuid, importHeadId,"Price");


            ZnodeLogging.LogMessage("Process for price refresh started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            SqlConnection conn = GetSqlConnection();
            try
            {
                SqlCommand cmd = new SqlCommand("Znode_ImportProcessData", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ImportHeadId", importHeadId);
                cmd.Parameters.AddWithValue("@TblGUID", tableGuid);
                cmd.Parameters.AddWithValue("@TouchPointName", GetCurrentMethod());
                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                }

                cmd.ExecuteReader();

                status = true;
                ZnodeLogging.LogMessage("Process for price refresh completed successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                /*New*/
                //GenerateLogs("Pricing Refresh Status", importHeadId);
                MoveFileToArchiveOnSuccess(EverestCSVNames.Pricing, "Price");
                return status;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                return false;
            }
        }

        //Refresh customer / contract price list from ERP to Znode on a scheduled basis.
        public override bool PricingCustomerPriceListRefresh() => true;

        //Get list or customer specifi price on real-time from ERP to Znode      
        public override bool GetPricing() => true;

        //Create Quote from Znode  to ERP   
        public override bool QuoteCreate() => true;

        //Get Quote History from ERP to Znode 
        public override bool QuoteHistory() => true;

        //Get Quote Details Status from ERP to Znode 
        public override bool QuoteDetailsStatus() => true;

        public override bool ShippingOptions() => true;
        public override bool ShippingNotification() => true;

        //Tax calculation from ERP to Znode 
        public override bool TaxCalculation() => true;
    }
}

